package com.pactera.adm.automation.rules;

import com.pactera.adm.automation.annotationImpl.AnnotationBeanPostProcessor;
import com.pactera.adm.automation.annotationImpl.SeleniumConfigImpl;
import com.pactera.adm.automation.annotationImpl.TargetURLImpl;
import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by David.Zheng on 28/04/2014.
 */
@Component
public class SeleniumConfigRule implements MethodRule
{
	private static final Logger logger = LoggerFactory.getLogger(SeleniumConfigRule.class);

	@Autowired
	private SeleniumConfigImpl seleniumConfigImpl;

	@Autowired
	private TargetURLImpl targetURLImpl;

	public final Statement apply(final Statement base,
	  final FrameworkMethod method, final Object target)
	{

		return new Statement()
		{
			@Override
			public void evaluate() throws Throwable
			{
				seleniumConfigImpl.processAnnotationByMethod(AnnotationBeanPostProcessor.getConfig(), method);
				targetURLImpl.processAnnotationByMethod(target, method);

				base.evaluate();
			}
		};
	}
}
