package com.pactera.adm.automation.annotation;

import java.lang.annotation.*;

/**
 * Created by David.Zheng on 1/05/2014.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface Page
{
}
